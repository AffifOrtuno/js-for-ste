import {expect} from 'chai';
import packageJson from "../package.json" with { type: "json" };

describe('Intro', () => {
  it('should invite participants', () => {
    console.log('Welcome to JS Mentoring program. You are ready to start the journey!');
  });

  it('package should have 3 authors', () => {
    expect(packageJson.authors, 'New author is not found').to.have.length(3);
  });

  it('added value should be an email', () => {
    const reg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    const matchResult = packageJson.authors[2]?.toLowerCase().match(reg);
    console.log(matchResult);

    expect(matchResult, 'Added value is not an email').not.to.equal(null);
    expect(matchResult, 'New author is not found').not.to.equal(undefined);
  });
});